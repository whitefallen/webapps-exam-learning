const path = require('path');
const outputDir = path.resolve(__dirname, 'public/js');
module.exports = {
    entry: path.resolve(__dirname, 'src/js/index.js'),
    output: {
        path: outputDir,
        filename: 'main.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }
        ]
    }
};
