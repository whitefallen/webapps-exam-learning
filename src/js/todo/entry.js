export default class Entry {

    constructor(...$value) {
        this.entryValue = $value[0];
        this.entryNumber = $value[1];
    }

    createContentEntry() {
        let componentEntry = document.createElement("div");
        componentEntry.className = "todo-component__content-entry";
        componentEntry.id = `entry-#${this.entryNumber}`;

        let componentEntryReadOnly = document.createElement("div");
        componentEntryReadOnly.className = "entry-readonly";

        let componentEntryEditable = document.createElement("div");
        componentEntryEditable.className = "entry-editable hidden";

        let componentEntryInput = document.createElement("input");
        componentEntryInput.type = "text";
        componentEntryInput.value = this.entryValue;
        componentEntryInput.dataset.entryId = `${this.entryNumber}`;

        let componentEntrySpan = document.createElement("span");
        componentEntrySpan.innerHTML  = this.entryValue;
        componentEntrySpan.dataset.entryId = `${this.entryNumber}`;

        let componentEntryEdit = document.createElement("button");
        componentEntryEdit.type = "button";
        componentEntryEdit.className = "edit-button";
        componentEntryEdit.dataset.entryId = `${this.entryNumber}`;

        componentEntryEdit.addEventListener("click", this.editEntryAction.bind(componentEntryEdit, componentEntryReadOnly, componentEntryEditable), false)

        let componentEntryEditIcon = document.createElement("i");
        componentEntryEditIcon.className = "fa fa-edit";
        componentEntryEdit.appendChild(componentEntryEditIcon);

        let componentEntryDelete = document.createElement("button");
        componentEntryDelete.type = "button";
        componentEntryDelete.className = "delete-button";
        componentEntryDelete.dataset.entryId = `${this.entryNumber}`;

        let componentEntryDeleteIcon = document.createElement("i");
        componentEntryDeleteIcon.className = "fa fa-trash";
        componentEntryDelete.appendChild(componentEntryDeleteIcon);

        componentEntryDelete.addEventListener("click", this.deleteEntryAction.bind(componentEntryDelete), false)

        let componentEntryConfirm = document.createElement("button");
        componentEntryConfirm.type = "button";
        componentEntryConfirm.className = "confirm-button";
        componentEntryConfirm.dataset.entryId = `${this.entryNumber}`;

        let componentEntryConfirmIcon = document.createElement("i");
        componentEntryConfirmIcon.className = "fa fa-check";
        componentEntryConfirm.appendChild(componentEntryConfirmIcon);

        componentEntryConfirm.addEventListener("click", this.confirmEditAction.bind(componentEntryConfirm, componentEntryReadOnly, componentEntryEditable), false);

        componentEntryEditable.appendChild(componentEntryInput);
        componentEntryEditable.appendChild(componentEntryConfirm);

        componentEntryReadOnly.appendChild(componentEntrySpan);
        componentEntryReadOnly.appendChild(componentEntryDelete);
        componentEntryReadOnly.appendChild(componentEntryEdit);

        componentEntry.appendChild(componentEntryReadOnly);
        componentEntry.appendChild(componentEntryEditable);

        return componentEntry;

    }

    deleteEntryAction() {
        document.getElementById(`entry-#${this.dataset.entryId}`).remove();
    }

    editEntryAction($spanDiv, $inputDiv) {
        $spanDiv.className = "entry-readonly hidden";
        $inputDiv.className = "entry-editable";
        document.getElementById("new-todo").disabled = true;
    }

    confirmEditAction($spanDiv, $inputDiv) {
        $spanDiv.className = "entry-readonly";
        $inputDiv.className = "entry-editable hidden";
        let entryId = this.dataset.entryId;
        let inputEle = document.querySelector(`input[data-entry-id="${entryId}"]`);
        let editValue = inputEle.value;
        let spanEle = document.querySelector(`span[data-entry-id="${entryId}"]`);
        spanEle.innerHTML = editValue;
        document.getElementById("new-todo").disabled = false;
    }

    render() {
        return this.createContentEntry();
    }
}
