import TodoHeader from "./header";
import Entry from "./entry";

export default class Todo {

    constructor($containerId, $headerTitle) {
        this.container = null;
        this.headerTitle = $headerTitle;
        this.entrysAmount = 0;
        if($containerId) {
            if(document.getElementById($containerId)) {
                this.container = document.getElementById($containerId);
                this.render();
            }
        }

    }

    createComponent () {
        let todoComponent = document.createElement("div");
        todoComponent.className = "todo-component";
        let header = new TodoHeader(this.headerTitle);
        let headerComponent = header.render();
        this.addHeaderInputEvents(header);
        todoComponent.appendChild(headerComponent);
        todoComponent.appendChild(this.createContentContainer());
        return todoComponent;
    }

    addHeaderInputEvents($headerComponent) {
        $headerComponent.addButton.addEventListener("click", this.createTodo.bind(this, $headerComponent), false)
    }

    createTodo($headerContainer) {
        let inputElement = $headerContainer.inputElement;
        if(inputElement.value) {
            this.entrysAmount++;
            let newEntry = new Entry(inputElement.value, this.entrysAmount);
            this.contentContainer.append(newEntry.render());
            inputElement.value = "";
        }
    }

    createContentContainer() {
        let contentContainer = document.createElement("div");
        contentContainer.className = "todo-component__content-container";
        this.contentContainer = contentContainer;
        return contentContainer;
    }

    render() {
        this.container.appendChild(this.createComponent());
    }

}
