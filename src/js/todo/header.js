export default class TodoHeader {

    constructor($headerTitle) {
        this.inputElement = null;
        this.addButton = null;
        this.headerTitle = $headerTitle;
    }

    render() {
        return this.createHeaderContainer();
    }

    createHeaderContainer() {
        let headerContainer = document.createElement("div");
        headerContainer.className = "todo-component__header-container";
        this.createHeaderContent(headerContainer);
        return headerContainer;
    }

    createHeaderContent($container) {
        let headerTitle = document.createElement("div");
        headerTitle.className = "todo-component__header-title";
        headerTitle.innerHTML = this.headerTitle;

        let headerInput = document.createElement("div");
        headerInput.className = "todo-component__header-input";

        let headerInputElement = this.createHeaderInputElement();
        let headerInputButton = this.createHeaderInputButton();

        this.inputElement = headerInputElement;
        this.addButton = headerInputButton;

        headerInput.append(headerInputElement);
        headerInput.append(headerInputButton);
        $container.append(headerTitle);
        $container.append(headerInput);
    }

    createHeaderInputElement() {
        let headerInputElement = document.createElement("input");
        headerInputElement.id = "new-todo";
        headerInputElement.name = "new-todo";
        headerInputElement.type = "text";
        headerInputElement.placeholder = this.headerTitle;
        return headerInputElement;
    }

    createHeaderInputButton() {
        let headerInputElementButton = document.createElement("button");
        headerInputElementButton.id = "new-todo-add";
        headerInputElementButton.name = "new-todo-add";
        headerInputElementButton.type = "button";
        headerInputElementButton.placeholder = this.headerTitle;

        let buttonIcon = document.createElement("i");
        buttonIcon.className = "fa fa-plus";

        headerInputElementButton.appendChild(buttonIcon);

        return headerInputElementButton;
    }
}
