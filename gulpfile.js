const {src, dest, watch, parallel, series} = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const webpack = require('webpack-stream');
const compiler = require('webpack');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');

let webpackTask = function () {
    return src('src/js/index.js')
        .pipe(webpack({
            config : require('./webpack.config.js'),
            devtool : 'source-map'
        }, compiler, function (err, stats) {

        }))
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write("."))
        .pipe(dest('public/js'));
}

let iconTask = function() {
    return src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(dest('public/assets/webfonts'));
}

let sassTask = function () {
    return src('src/scss/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(dest('public/css'))
}

let watchTask = function () {
    watch('src/js/**/*.js', webpackTask);
    watch('src/scss/**/*.scss', sassTask);
}

let deployTask = series(webpackTask,sassTask,iconTask);

let defaultTask = parallel(watchTask, series(webpackTask,sassTask));

exports.default = defaultTask;
exports.deploy = deployTask;
exports.webpack = webpackTask;
exports.watch = watchTask;
exports.sass = sassTask;
exports.icon = iconTask;
